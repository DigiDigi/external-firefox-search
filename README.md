# External Firefox Search

App for externally launching Firefox searches in private browsing mode.

Optionally you can set your firefox profile path in config.cfg.

![Screenshot](effs.png)

m5x7 font: https://managore.itch.io/m5x7
