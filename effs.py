#!/usr/bin/env python3
# coding: utf-8

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from gi.repository import Gio
import subprocess
import configparser

class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_title("External Firefox Search")
        self.vbox = Gtk.VBox(False, 2)
        self.vbox.show()
        self.add(self.vbox)

        # Import widget stuff from glade >
        self.gladefile = "effs.glade"
        self.builder = Gtk.Builder()  # Used to build gui objects from our glade file.
        self.builder.add_from_file(self.gladefile)  # Add our glade file with our filename.
        self.builder.connect_signals(self)  # Connect the signals from our glade file to our callbacks in this object.

        self.mainelement = self.builder.get_object("mainelement")
        self.vbox.add(self.mainelement)

        # list of entry boxes.
        self.entryvbox = self.mainelement.get_children()[0].get_children()[1]
        self.entryboxes = self.entryvbox.get_children()
        entryid = 0
        for entrybox in self.entryboxes:
            entrybox.pyid = entryid
            entrybox.connect("activate", self.on_activate)
            entryid += 1
        self.ffbutton = self.mainelement.get_children()[1]
        self.ffbutton.connect("clicked", self.on_ffbutton)

        # CSS styling and settings >
        settings = Gtk.Settings.get_default()
        settings.props.gtk_button_images = True
        #settings.set_string_property("gtk-theme-name", "effs", "")
        self.style_provider = Gtk.CssProvider()
        css = open('effs.css', 'rb')
        self.css_data = css.read()
        css.close()
        self.style_provider.load_from_data(self.css_data)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), self.style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        config = configparser.ConfigParser()
        config.read('config.cfg')
        if 'Firefox' in config:
            if 'profilepath' in config['Firefox']:
                self.profilepath = config['Firefox']['profilepath']
        else:
            self.profilepath = ''

    def on_activate(self, widget):
        pyid = widget.pyid
        if pyid == 0: # DuckDuckGo
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://duckduckgo.com/?q=" + searchtext + "&t=canonical"
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 1: # Google
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://www.google.com/search?client=ubuntu&channel=fs&q=" + searchtext + "&ie=utf-8&oe=utf-8"
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 2: # Wikipedia
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '_')
            searchtext = "https://en.wikipedia.org/wiki/" + searchtext + ""
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 3: # Wiktionary
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '_')
            searchtext = "https://en.wiktionary.org/wiki/" + searchtext + ""
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 4: # Soundcloud
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://soundcloud.com/search?q=" + searchtext + ""
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 5: # Twitter
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://twitter.com/search?q=" + searchtext + "&partner=Firefox&source=desktop-search"
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 6: # Github
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://github.com/search?q=" + searchtext + "&ref=opensearch"
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 7: #Amazon
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://www.amazon.com/s?k=" + searchtext + "&link_code=qs&sourceid=Mozilla-search"
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 8: #Ebay
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://www.ebay.com/sch/i.html?_nkw=" + searchtext + ""
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 9: #Youtube
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://www.youtube.com/results?search_query=" + searchtext + "&page=&utm_source=opensearch"
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])

        elif pyid == 10: #Reddit
            searchtext = widget.get_text()
            searchtext = searchtext.replace(' ', '+')
            searchtext = "https://www.reddit.com/search/?q=" + searchtext + ""
            if self.profilepath == '':
                subprocess.Popen(["firefox", "--private-window", searchtext])
            else:
                subprocess.Popen(["firefox", "--profile", self.profilepath[1:-1], "--private-window", searchtext])
        widget.set_text('')
        self.destroy()

    def on_ffbutton(self, widget):
        subprocess.Popen(["firefox", "--private-window", "about:blank"])
        self.destroy()

win = MainWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
